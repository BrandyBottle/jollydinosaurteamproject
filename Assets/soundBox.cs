﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class soundBox : MonoBehaviour
{
    public bool enteredClass = false;

    lecturer_footsteps lt = new lecturer_footsteps();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    
    public void OnTriggerEnter(Collider collider)
    {
        if(collider.tag == "Player")
        {
                FindObjectOfType<lecturer_footsteps>().insideClassroom();
           
        }

        if(collider.tag == "Lecturer" && collider.tag == "Player")
        {
            FindObjectOfType<lecturer_footsteps>().outsideClassroom();
        }


    }

    public void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "Player")
        {
            //FindObjectOfType<AudioManager>().Play("Thanks");
            FindObjectOfType<lecturer_footsteps>().outsideClassroom();
        }
    }

}
