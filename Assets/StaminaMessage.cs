﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class StaminaMessage : MonoBehaviour
{
    public FirstPersonController controller;
    public MessageBubbleManager bubble;

    bool reset = true;
    // Update is called once per frame
    void Update()
    {
        if(controller.ifFatigued() && reset)
        {
            reset = false;
            string[] messages = { "Fitness Tracker Notification: Pal, your body was made for many things, running for long periods wasn't one of them. Take a breather" };
            bubble.createMessage(messages, 2.5f);
        }
        else if(!controller.ifFatigued() && !reset)
        {
            reset = true;
        }
    }
}
