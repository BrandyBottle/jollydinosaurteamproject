﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorCollider : MonoBehaviour
{
    public float timeLeft = 0;
    public RaycastHit hit;
    public Transform currentDoor;
    public bool open;
    public bool isOpeningDoor;
    public Transform cam;
    public LayerMask mask;
    bool triggered = false;

    // Update is called once per frame
    void Update()
    {
        if (triggered)
            OpenDoor();
        else
            CloseDoor();

    }


    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Player")
        {
            triggered = true;
            OpenDoor();
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            triggered = false;
            CloseDoor();
        }
    }

    void checkDoor()
    {
            isOpeningDoor = true;
            if (this.transform.localRotation.eulerAngles.y > 45)
            {
                triggered = false;
            }
                
            currentDoor = this.transform;
        
    }


    public void OpenAndCloseDoor()
    {
        timeLeft += Time.deltaTime;
        if (open)
            currentDoor.localRotation = Quaternion.Slerp(currentDoor.localRotation, Quaternion.Euler(0, 0, 0), timeLeft);
        else
            currentDoor.localRotation = Quaternion.Slerp(currentDoor.localRotation, Quaternion.Euler(0, 90, 0), timeLeft);

        if (timeLeft > 1f)
        {
            timeLeft = 0;
            isOpeningDoor = false;

        }
    }

    void OpenDoor()
    {
        timeLeft += Time.deltaTime;
        currentDoor.localRotation = Quaternion.Slerp(currentDoor.localRotation, Quaternion.Euler(0, 90, 0), timeLeft);

        if (timeLeft > 1f)
        {
            timeLeft = 0;
            isOpeningDoor = false;
        }
    }

    void CloseDoor()
    {
        timeLeft += Time.deltaTime;
        currentDoor.localRotation = Quaternion.Slerp(currentDoor.localRotation, Quaternion.Euler(0, 0, 0), timeLeft);

        if (timeLeft > 1f)
        {
            timeLeft = 0;
            isOpeningDoor = false;
        }
    }
}
