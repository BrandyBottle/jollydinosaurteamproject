﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MessageBubbleManager : MonoBehaviour
{
    public GameObject textBubble = null;
    public RectTransform canvas;

    GameObject clone;
    GameObject[] messages = { null, null, null };
    bool hasMessage = false;
    public float delayTime, timer, onscreenTime = 200f;
    float padding = 25f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (hasMessage)
        {
            if(timer < onscreenTime)
            {
                timer += Time.deltaTime;
                if (!messages[0].activeSelf && messages[0] != null)
                {
                    messages[0].SetActive(true);
                    FindObjectOfType<AudioManager>().Play("vibration");
                    FindObjectOfType<AudioManager>().Play("pop");
                }
                else
                {
                    positionMessages(0);
                }
                if (messages[1] != null)
                {
                    if (!messages[1].activeSelf && timer > delayTime)
                    {
                        messages[1].SetActive(true);
                        FindObjectOfType<AudioManager>().Play("vibration");
                        FindObjectOfType<AudioManager>().Play("pop");
                    }
                    else
                    {
                        positionMessages(1);
                    }
                }
                if (messages[2] != null)
                {
                    if (!messages[2].activeSelf && timer > delayTime * 2)
                    {
                        messages[2].SetActive(true);
                        FindObjectOfType<AudioManager>().Play("vibration");
                        FindObjectOfType<AudioManager>().Play("pop");
                    }
                    else
                    {
                        positionMessages(2);
                    }
                }
            }
            
            
            // Turn off and Reset Messages
            if(timer > onscreenTime)
            {
                messages[0].SetActive(false);
                messages[1].SetActive(false);
                messages[2].SetActive(false);
                messages[0] = null;
                messages[1] = null;
                messages[2] = null;
                hasMessage = false;
            }

        }
    }

    public void createMessage(string[] message, float time)
    {
        //clearMessages();
        float spacing = 0;

        for (int i = 0; i < message.Length; i++)
        {
           /* if (i > 0)
            {
                // Get the transform of the previous message
                RectTransform rect = messages[i - 1].GetComponent<RectTransform>();
                spacing = rect.localPosition.y - padding;
                Debug.Log(rect.rect.height);
            }
            else
            {
                spacing = 120;
            }*/
            clone = Instantiate(textBubble, transform.position, Quaternion.identity, canvas);
            //clone.transform.localPosition = new Vector3(-190, spacing, 0);
            clone.GetComponentInChildren<Text>().text = message[i];
            //clone.GetComponent<RectTransform>().transform.SetPositionAndRotation(new Vector3(-190, y, 0), Quaternion.identity);
            messages[i] = clone;
        }

        hasMessage = true;
        delayTime = time;
        timer = 0f;
    }

    void positionMessages(int messageIndex)
    {

        float spacing = 0;
        if(messageIndex == 0)
        {
            messages[messageIndex].transform.localPosition = new Vector3(-220, -150, 0);
            
        }
        else
        {
            RectTransform rect = messages[messageIndex - 1].GetComponent<RectTransform>();
            spacing = rect.localPosition.x + rect.rect.width + padding;
            Debug.Log(rect.rect.height);
            messages[messageIndex].transform.localPosition = new Vector3(spacing, -150, 0);
            
        }
    }

    void clearMessages()
    {
        if(hasMessage)
        {
            messages[0].SetActive(false);
            messages[1].SetActive(false);
            messages[2].SetActive(false);
            messages[0] = null;
            messages[1] = null;
            messages[2] = null;
        }
        
    }
}
