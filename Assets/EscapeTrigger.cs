﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EscapeTrigger : MonoBehaviour
{

    // In future this script needs bump you to a new scene. The basement scene.
    public Text uiText;

    // In future, the LevelStateManager should activate this trigger opposed to referencing the LevelStateManager
    public LevelStateManager levelStateManager;

    void OnTriggerEnter(Collider col)
    {
        if (levelStateManager.getCurrentState() == 4)
        {
            if (col.gameObject.tag == "Player")
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }
        }
    }
}
