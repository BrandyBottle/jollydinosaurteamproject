﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToiletMan : MonoBehaviour
{
    void OnTriggerEnter (Collider col)
    {
        if(col.gameObject.name == "BogRoll")
        {
            FindObjectOfType<AudioManager>().Play("Thanks");
        }
    }
}
