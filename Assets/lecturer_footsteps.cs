﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[RequireComponent(typeof(AudioSource))]
public class lecturer_footsteps : MonoBehaviour
{
    [SerializeField] AudioClip inside_footsteps;
    [SerializeField] AudioClip outside_footsteps;

    public GameObject footStepsBox;


    AudioSource f_AudioSource;

   

    public void Start()
    {
        f_AudioSource = GetComponent<AudioSource>();

        footStepsBox = GetComponent<GameObject>();

        f_AudioSource.clip = inside_footsteps;

        f_AudioSource.Play();
    }

    void Update()
    {
        /*
        if(Input.GetKeyDown("space"))
        {
            FindObjectOfType<AudioManager>().Play("pop");
            g();
        }*/


    }

    public void insideClassroom()
    {
        
        f_AudioSource.clip = outside_footsteps;

        f_AudioSource.Play();
    }

    public void outsideClassroom()
    {
        f_AudioSource.clip = inside_footsteps;

        f_AudioSource.Play();
    }
}
