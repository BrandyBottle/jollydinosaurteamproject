﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TorchLight : MonoBehaviour {

    int batteryLife;
    float timer;
    bool charging = false;

    public GameObject text;
    public Text batteryPercentage;



	// Use this for initialization
	void Start ()
    {
        batteryLife = 99;
        

        // Start the game with the torch light disabled
        GetComponent<Light>().enabled = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        
        if(GetComponent<Light>().enabled && batteryLife > 0 && !charging)
        {
            timer += Time.deltaTime;
            if(timer > 1.5f)
            {
                timer = 0f;
                batteryLife--;
                if (batteryLife <= 0)
                {
                    GetComponent<Light>().enabled = false;
                    batteryPercentage.color = Color.red;
                }
                else
                {
                    batteryPercentage.color = Color.white;
                }
            }

            
            
        }
		if(Input.GetKeyDown("space") || Input.GetKeyDown("joystick button 3"))
        {
            if(batteryLife > 0)
            {
                GetComponent<Light>().enabled = !GetComponent<Light>().enabled;
            } 
        }

        if (charging)
        {
            Debug.Log("Charging");
            text.SetActive(true);
            timer += Time.deltaTime;
            batteryPercentage.color = Color.white;

            if (timer > 5f)
            {
                batteryLife++;
                if(batteryLife > 99)
                {
                    batteryLife = 99;
                }
            }
        }
        else
        {
            text.SetActive(false);
        }

        batteryPercentage.text = batteryLife.ToString();
    }

    public void chargePhone()
    {
        if(batteryLife <= 99)
        {
            timer = 0f;
            charging = true;
        }    
    }

    public void stopCharging()
    {
        charging = false;
    }
}
