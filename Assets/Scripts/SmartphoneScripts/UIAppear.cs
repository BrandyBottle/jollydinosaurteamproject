﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIAppear : MonoBehaviour
{
    [SerializeField] private Image customImage;
    [SerializeField] private GameObject mesh;

    void Start()
    {
        mesh.GetComponent<Renderer>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space") || Input.GetKeyDown("joystick button 3"))
        {
            //customImage.enabled = !customImage.enabled;

            mesh.GetComponent<Renderer>().enabled = !mesh.GetComponent<Renderer>().enabled;

            FindObjectOfType<AudioManager>().Play("toggle_light_jingle");
        }
    }
}
