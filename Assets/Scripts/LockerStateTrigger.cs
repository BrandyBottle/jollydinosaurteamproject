﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockerStateTrigger : MonoBehaviour
{
    public delegate void StateChange(int state);
    public static event StateChange stateChange;

    public bool hasCombination = false;
    bool stateChanged = false;

    public Animator ani;
    public GameObject lecturer;

    [SerializeField] private GameObject mesh;

    void onStart()
    {
        ani.enabled = false;
        mesh.GetComponent<Renderer>().enabled = false;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            if(hasCombination)
            {
                // Change to Upload state
                stateChange(3);

                FindObjectOfType<AudioManager>().Play("got_password");
                mesh.GetComponent<Renderer>().enabled = true;
            }
            else if(!hasCombination && !stateChanged)
            {
                // Change to WifiState
                stateChanged = true;
                stateChange(1);
                FindObjectOfType<AudioManager>().Play("blackout");

                turnOutLights();
                lecturer.SetActive(true);
                ani.enabled = true;

                
            }
        }
    }

    void turnOutLights()
    {
        GameObject[] lights = GameObject.FindGameObjectsWithTag("Light");

        for(int i = 0; i < lights.Length; i++)
        {
            lights[i].SetActive(false);
        }
        
    }

    public void getCombination()
    {
        hasCombination = true;
    }
}
