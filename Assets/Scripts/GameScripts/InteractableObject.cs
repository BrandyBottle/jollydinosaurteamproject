﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class InteractableObject : MonoBehaviour {


    public enum ColliderType { Box, Sphere };

    public ColliderType colliderType;
    public Text uiText;
    public Image uiImage;
    public string uiName;
    public bool holding = false;
    [HideInInspector] public bool triggered;

    void Awake()
    {
        switch (colliderType)
        {
            case ColliderType.Box:
                BoxCollider bc = gameObject.AddComponent<BoxCollider>() as BoxCollider;
                break;

            case ColliderType.Sphere:
                SphereCollider sc = gameObject.AddComponent<SphereCollider>() as SphereCollider;
                break;

            default:
                BoxCollider def = gameObject.AddComponent<BoxCollider>() as BoxCollider;
                break;
        }
    }

    public void triggeredObject()
    {
        if(!holding)
        {
            //uiText.text = "Pick up " + this.uiName;
            uiImage.enabled = true;
            triggered = true;
        }
        
    }

    public void untriggeredObject()
    {
        triggered = false;
        uiImage.enabled = false;
        //uiText.text = " ";
    }


}
