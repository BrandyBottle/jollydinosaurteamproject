﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class WifiSpot : MonoBehaviour {

    bool isActive;
    public bool triggered;
    WifiSpotManager wifiManager;

    [SerializeField] private GameObject bufferSymbol;

    // Use this for initialization
    void Awake () {
        isActive = false;
        triggered = false;
        bufferSymbol.GetComponent<Renderer>().enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
	}

    void OnTriggerEnter(Collider collider)
    {
        if(collider.tag == "Player" && isActive)
        {
            triggered = true;

            bufferSymbol.GetComponent<Renderer>().enabled = true;

            FindObjectOfType<AudioManager>().Play("confirmed");
        }
    }

    void OnTriggerExit(Collider collider)
    {
        bufferSymbol.GetComponent<Renderer>().enabled = false;
    }

    public void activate()
    {
        isActive = true;
        triggered = false;
    }

    public void deactivate()
    {
        isActive = false;
        triggered = false;
    }
}
