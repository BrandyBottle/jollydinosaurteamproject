﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

[System.Serializable]

public class CollectableObject : InteractableObject
{

    public Transform playerPosition;
    public PlayerInventory inventory;
    public int id;

    bool firstTime = true;

    public MessageBubbleManager textBubble;

    void Start()
    {
        playerPosition = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        inventory = GameObject.Find("Inventory").GetComponent<PlayerInventory>();
        uiText = GameObject.Find("InteractableText").GetComponent<Text>();
        uiImage = GameObject.Find("Crosshair").GetComponent<Image>();
        textBubble = GameObject.Find("MessageBubbleManager").GetComponent<MessageBubbleManager>();
    }

    void OnMouseDown()
    {
        float distance = Vector3.Distance(this.transform.position, playerPosition.transform.position);
        if (distance <= 2.55f && triggered)
        {
            // Collect the object
            gameObject.SetActive(false);

            if(firstTime && id == 1)
            {
                firstTime = false;
                string[] messages = { "QR Scanner Notification: Ah a simple cuppa joe! Help give you quick SPEED UP when you need one!"};
                textBubble.createMessage(messages, 2.5f);
            }

            inventory.pickUp(id);
        }
    }
}
