﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{
    public float timeLeft = 0;
    public RaycastHit hit;
    public Transform currentDoor;
    public bool open;
    public bool isOpeningDoor;
    public Transform cam;
    public LayerMask mask;


    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && timeLeft == 0.0f)
            checkDoor();
        if (isOpeningDoor)
            OpenAndCloseDoor();

    }
    
    public void checkDoor()
    {
        if (Physics.Raycast(cam.position, cam.forward, out hit, 5, mask))
        {
            open = false;
            if (hit.transform.localRotation.eulerAngles.y > 45)
                open = true;

            isOpeningDoor = true;
            currentDoor = hit.transform;
        }
    }
    public void OpenAndCloseDoor()
    {
        timeLeft += Time.deltaTime;
        if (open)
            currentDoor.localRotation = Quaternion.Slerp(currentDoor.localRotation, Quaternion.Euler(0, 0, 0), timeLeft);
        else
            currentDoor.localRotation = Quaternion.Slerp(currentDoor.localRotation, Quaternion.Euler(0, 90, 0), timeLeft);

        if (timeLeft > 1f)
        {
            timeLeft = 0;
            isOpeningDoor = false;

        }
    }
}
