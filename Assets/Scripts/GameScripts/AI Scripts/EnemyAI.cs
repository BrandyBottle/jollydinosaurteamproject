﻿using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyAI : MonoBehaviour
{
    private StateMachine stateMachine = new StateMachine();
    private Renderer rend;

    public GameObject player;
    public float fieldOfVeiwAngle = 50f;
    public Transform[] points;

    public const int AttackDistance = 10;
    public const int ChaseDisance = 50;
    public const int SuspicionDisance = 100;

    // Bit shift the index of the layer (8) to get a bit mask
    //private int layerMask = 1 << 8;

    //layer = ~layer;
    //private int layerMask = 8;

    private NavMeshAgent navMeshAgent;
    public Animation anim;
    public PlayerDamage playerHealth;


    private bool playerInSight;
    private float distanceFromPlayer;
    private bool playerHit;
    private bool inRange;
    private Vector3 itemPositon;



    private void Start()
    {

        navMeshAgent = this.GetComponent<NavMeshAgent>();
        anim = GetComponentInChildren<Animation>();
        rend = GetComponent<Renderer>();
        player = GameObject.FindGameObjectWithTag("Player");

        stateMachine.ChangeState(new PatrolState(ref navMeshAgent, ref points, fieldOfVeiwAngle,
            ref player, /*layerMask,*/ ref rend, ref playerInSight, PlayerSeen));
        //stateMachine.ChangeState(new TestState(this.navMeshAgent, this.points, player, ref rend));
    }

    private void Update()
    {
        this.stateMachine.ExecuteStateUpdate();
    }

    public void PlayerSeen(PatrolResults patrolResults)
    {
        playerInSight = patrolResults.playerSeen;
        distanceFromPlayer = patrolResults.distanceFromPlayer;
        if (playerInSight)
        {
            if (distanceFromPlayer <= AttackDistance)
            {
                stateMachine.ChangeState(new AttackState(ref navMeshAgent, fieldOfVeiwAngle, ref player, /*layerMask,*/ ref rend, ref playerInSight, playerHealth, PlayerAttacked));
                anim.Stop();
                anim.Play("Hit Reaction");
            }

            else if (distanceFromPlayer <= ChaseDisance)
            {
                stateMachine.ChangeState(new ChaseState(ref navMeshAgent, fieldOfVeiwAngle, ref player, /*layerMask,*/ref rend, ref playerInSight, PlayerLosted));
                //anim.Play("Alert");
            }

            else if (distanceFromPlayer <= SuspicionDisance)
            {
                stateMachine.ChangeState(new SuspicionState(ref navMeshAgent, fieldOfVeiwAngle, ref player, ref rend, ref playerInSight, distanceFromPlayer, PlayerLosted));
                anim.Stop();
                anim.Play("Walk");
            }
        }
        else
            //this.stateMachine.ExecuteStateUpdate();
            stateMachine.ChangeState(new TargetedPatrolState(ref navMeshAgent, ref points, fieldOfVeiwAngle,
                ref player, ref rend, ref playerInSight, PlayerSeen));
    }

    public void PlayerAttacked(AttackState.AttackResults attackResults)
    {
        playerInSight = attackResults.playerSeen;
        distanceFromPlayer = attackResults.distanceFromPlayer;
        playerHit = attackResults.hit;

        if (playerHit)
        {
            anim.Stop();
            anim.Play("Shocked");
            stateMachine.ChangeState(new DazedState(ref navMeshAgent, AIDazed));
        }
        else if (playerInSight)
        {
            if (distanceFromPlayer <= AttackDistance)
                Debug.Log("Hit Reaction");
            //this.stateMachine.ExecuteStateUpdate();

            else if (distanceFromPlayer <= ChaseDisance)
            {
                stateMachine.ChangeState(new ChaseState(ref navMeshAgent, fieldOfVeiwAngle, ref player, /*layerMask,*/ref rend, ref playerInSight, PlayerLosted));
                //anim.Play("Alert");
            }
        }
        else
        {
            stateMachine.ChangeState(new SuspicionState(ref navMeshAgent, fieldOfVeiwAngle, ref player, ref rend,
                    ref playerInSight, distanceFromPlayer, PlayerLosted));
            anim.Stop();
            anim.Play("Walk");
        }

        //stateMachine.ChangeState(new PatrolState(ref navMeshAgent, ref points, fieldOfVeiwAngle,
        //    ref player, ref rend, ref playerInSight, PlayerSeen));
    }

    public void PlayerLosted(SuspicionState.SuspicionResults suspicionResults)
    {
        playerInSight = suspicionResults.playerSeen;
        distanceFromPlayer = suspicionResults.distanceFromPlayer;

        if (playerInSight)
        {
            if (distanceFromPlayer <= AttackDistance)
            {
                stateMachine.ChangeState(new AttackState(ref navMeshAgent, fieldOfVeiwAngle, ref player, /*layerMask,*/ ref rend, ref playerInSight, playerHealth, PlayerAttacked));
                anim.Stop();
                anim.Play("Attack");
            }
            else if (distanceFromPlayer <= ChaseDisance)
            {

                stateMachine.ChangeState(new ChaseState(ref navMeshAgent, fieldOfVeiwAngle, ref player, /*layerMask,*/ref rend, ref playerInSight, PlayerLosted));
                //anim.Play("Alert");
            }
            else
            {
                Debug.Log("Suspicion");
                anim.Stop();
                anim.Play("Walk");
            }
        }
        else
        {
            stateMachine.ChangeState(new TargetedPatrolState(ref navMeshAgent, ref points, fieldOfVeiwAngle,
               ref player, ref rend, ref playerInSight, PlayerSeen));
            anim.Stop();
            anim.Play("Walk");
        }

    }

    public void PlayerLosted(ChaseState.ChaseResults chaseResults)
    {
        playerInSight = chaseResults.playerSeen;
        distanceFromPlayer = chaseResults.distanceFromPlayer;

        if (playerInSight)
        {
            if (distanceFromPlayer <= AttackDistance)
            {
                stateMachine.ChangeState(new AttackState(ref navMeshAgent, fieldOfVeiwAngle, ref player, /*layerMask,*/ ref rend, ref playerInSight, playerHealth, PlayerAttacked));
                anim.Stop();
                anim.Play("Hit Reaction");
            }
            else if (distanceFromPlayer <= ChaseDisance)
            {
                Debug.Log("Chase");
                anim.Stop();
                anim.Play("Walk");
            }

            //stateMachine.ChangeState(new ChaseState(ref navMeshAgent, fieldOfVeiwAngle, ref player, /*layerMask,*/ref rend, ref playerInSight, PlayerLosted));
            //else
            //    stateMachine.ChangeState(new SuspicionState(ref navMeshAgent, fieldOfVeiwAngle, ref player, ref rend, ref playerInSight, distanceFromPlayer, PlayerLosted));
        }
        else
        {
            stateMachine.ChangeState(new SuspicionState(ref navMeshAgent, fieldOfVeiwAngle, ref player, ref rend,
                    ref playerInSight, distanceFromPlayer, PlayerLosted));
            anim.Stop();
            anim.Play("Walk");
        }

        //stateMachine.ChangeState(new PatrolState(ref navMeshAgent, ref points, fieldOfVeiwAngle,
        //    ref player, ref rend, ref playerInSight, PlayerSeen));
    }

    public void AIDazed(DazedResults dazedResults)
    {
        bool undazed = dazedResults.Undazed;
        if (undazed)
        {
            stateMachine.ChangeState(new TargetedPatrolState(ref navMeshAgent, ref points, fieldOfVeiwAngle,
                ref player, ref rend, ref playerInSight, PlayerSeen));
            anim.Stop();
            anim.Play("Walk");
        }
        else
            this.stateMachine.ExecuteStateUpdate();
    }

    //public static void ThrowableObjectResults(bool InRange, Vector3 ObjPosition) 
    //{
    //    if (InRange)
    //    {
    //        Debug.Log("********** 5 *************");

    //        EnemyAI enemyAI = new EnemyAI();
    //        enemyAI.ChangeState(ObjPosition, InRange);
    //    }
    //}
    //public static void ThrowableObjectResults(bool Hit)
    //{
    //    if (Hit)
    //    {
    //        Debug.Log("********** 6 *************");

    //        EnemyAI enemyAI = new EnemyAI();
    //        enemyAI.ChangeState(Hit);
    //    }
    //}

    public void ItemInRange(SoundResults soundResults)
    {
        Debug.Log("********** 5 *************");

        inRange = soundResults.inRange;
        itemPositon = soundResults.ItemPosition;

        if (playerInSight)
        {
            if (distanceFromPlayer <= AttackDistance)
            {
                stateMachine.ChangeState(new AttackState(ref navMeshAgent, fieldOfVeiwAngle, ref player, /*layerMask,*/ ref rend, ref playerInSight, playerHealth, PlayerAttacked));
                anim.Stop();
                anim.Play("Hit Reaction");
            }

            else if (distanceFromPlayer <= ChaseDisance)
            {
                stateMachine.ChangeState(new ChaseState(ref navMeshAgent, fieldOfVeiwAngle, ref player, /*layerMask,*/ref rend, ref playerInSight, PlayerLosted));
                //anim.Play("Alert");
            }

            else if (distanceFromPlayer <= SuspicionDisance)
            {
                anim.Stop();
                anim.Play("Walk");
                stateMachine.ChangeState(new SuspicionState(ref navMeshAgent, fieldOfVeiwAngle, ref player, ref rend, ref playerInSight, distanceFromPlayer, PlayerLosted));
                
            }
        }
        else
        {
            Debug.Log("********** 555 *************");
            anim.Stop();
            anim.Play("Search");
            stateMachine.ChangeState(new ItemSuspicionState(ref navMeshAgent, fieldOfVeiwAngle, ref player, ref playerInSight, distanceFromPlayer, itemPositon, PlayerLosted));
            
        }

    }
    public void AIItemHit()
    {
        anim.Stop();
        anim.Play("Shocked");
        stateMachine.ChangeState(new DazedHitState(ref navMeshAgent, AIDazed));

    }


}

public class SoundResults
{
    public Vector3 ItemPosition;
    public bool inRange;

    public SoundResults(Vector3 itemPosition, bool inRange)
    {
        this.ItemPosition = itemPosition;
        this.inRange = inRange;
    }
}