﻿using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class BasementAI : MonoBehaviour
{
    private StateMachine stateMachine = new StateMachine();
    private Renderer rend;

    public GameObject player;
    public float fieldOfVeiwAngle = 50f;
    public Transform[] points;

    public const int AttackDistance = 10;
    public const int ChaseDisance = 50;
    public const int SuspicionDisance = 100;

    // Bit shift the index of the layer (8) to get a bit mask
    //private int layerMask = 1 << 8;

    //layer = ~layer;
    //private int layerMask = 8;

    private NavMeshAgent navMeshAgent;
    public Animation anim;
    private PlayerDamage playerHealth = new PlayerDamage();


    private bool playerInSight;
    private float distanceFromPlayer;
    private bool playerHit;



    private void Start()
    {

        navMeshAgent = this.GetComponent<NavMeshAgent>();
        anim = GetComponentInChildren<Animation>();
        player = GameObject.FindGameObjectWithTag("Player");

        stateMachine.ChangeState(new BasementChaseState(ref navMeshAgent, ref points,
            ref player));
        anim.Play("Walking");
    }

    private void FixedUpdate()
    {
        this.stateMachine.ExecuteStateUpdate();
    }
    
}