﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.AI;


//Suspicion is the state after patrol when the player is in line of sight of the ai.
// the ai should leave the path investigate what it is and move slightly faster. if it gets close
// enough then it enters the chase state.

public class SuspicionState : MonoBehaviour, IState
{
    private float timer = 0.0f;
    public float speed = 72f;
    private NavMeshAgent lecturer;
    private float fieldOfViewAngle;
    private GameObject player;

    //private int layerMask;
    private Renderer rend;

    private bool playerInSight;
    private float distanceFromPlayer;
    private System.Action<SuspicionResults> suspicionResultsCallback;
    private Vector3 playerLastSighting;
    private Vector3 ObjectPosition;
    private bool InRange = false;


    public LevelStateManager levelStateManager;

    public SuspicionState(ref NavMeshAgent lecturer, float fieldOfViewAngle, ref GameObject player,
        ref Renderer rend, ref bool playerInSight, float distanceFromPlayer, Action<SuspicionResults> suspicionResultsCallback)
    {
        this.lecturer = lecturer;
        this.fieldOfViewAngle = fieldOfViewAngle;
        this.player = player;
        //this.layerMask = layerMask;
        this.rend = rend;
        this.playerInSight = playerInSight;
        this.distanceFromPlayer = distanceFromPlayer;
        this.suspicionResultsCallback = suspicionResultsCallback;
    }

    public void OnStateEnter()
    {
        playerInSight = true;
        playerLastSighting = player.transform.position;

        lecturer.destination = playerLastSighting;
        lecturer.speed = 4f;
        Debug.Log("Entering Suspiicion State");
        rend.material.color = new Color32(243, 243, 21, 1);

        /*
        if (levelStateManager.getCurrentState() != 4)
        {*/
            FindObjectOfType<AudioManager>().Play("caution_music");
            FindObjectOfType<AudioManager>().Stop("fade_in_ambience");
        //}
        
    }

    public void OnStateExit()
    {
        Debug.Log("Exiting Suspiicion State");

        /*if (levelStateManager.getCurrentState() != 4)
        {*/
            FindObjectOfType<AudioManager>().Stop("caution_music");
            FindObjectOfType<AudioManager>().Play("fade_in_ambience");
        //}
        
        
    }

    public void OnStateUpdate()
    {
        Vector3 direction = player.transform.position - lecturer.transform.position;
        float angle = Vector3.Angle(direction, lecturer.transform.forward);
        RaycastHit hit;

        // get AI to go the the direction of what they seen.
        if (angle >= -fieldOfViewAngle && angle <= fieldOfViewAngle &&
            Physics.Raycast(lecturer.transform.position, direction.normalized,
            out hit, EnemyAI.SuspicionDisance))
        {
            if (hit.transform.gameObject.CompareTag("Player"))
            {
                Debug.DrawRay(lecturer.transform.position, direction.normalized * hit.distance,
                Color.red);

                playerLastSighting = player.transform.position;

                lecturer.destination = playerLastSighting;

                if (Vector3.Distance(lecturer.transform.position, player.transform.position) <= EnemyAI.ChaseDisance)
                {
                    distanceFromPlayer = Vector3.Distance(lecturer.transform.position, player.transform.position);

                    var suspicionResults = new SuspicionResults(playerInSight, distanceFromPlayer);
                    suspicionResultsCallback(suspicionResults);
                }

                //lecturer.transform.position = Vector3.MoveTowards(lecturer.transform.position,
                //    playerLastSighting, lecturer.speed * Time.deltaTime);
                //lecturer.transform.LookAt(player.transform.position);
            }
        }
        else if (InRange)
        {
            Debug.Log("********** 7777 *************");

            lecturer.destination = ObjectPosition;
        }

        if (lecturer.remainingDistance < 5f)
        {
            timer += Time.deltaTime;

            //Should be a look around animation
            if (timer <= 5f)
            {

                lecturer.transform.Rotate(Vector3.up, speed * Time.deltaTime);

                //lecturer.transform.LookAt(player.transform.position);
                //Debug.Log(timer + " Time dt" + Time.deltaTime);

            }
            else
            {
                lecturer.transform.LookAt(Vector3.forward);
                Debug.Log("forward");
                playerInSight = false;
                distanceFromPlayer = Vector3.Distance(lecturer.transform.position, player.transform.position);

                var suspicionResults = new SuspicionResults(playerInSight, distanceFromPlayer);

                suspicionResultsCallback(suspicionResults);
            }
            //Debug.Log("time" + timer);
        }
    }

    public class SuspicionResults
    {
        public bool playerSeen;
        public float distanceFromPlayer;

        //return distance?
        public SuspicionResults(bool playerSeen, float distanceFromPlayer)
        {
            this.playerSeen = playerSeen;
            this.distanceFromPlayer = distanceFromPlayer;
        }
    }
}