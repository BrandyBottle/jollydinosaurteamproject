﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.AI;

// the attack comes only when the player is close enough to actually attack normally after the
// chase state. should enter dazed state after an attack.

public class AttackState : IState
{
    private const int MaxDistance = 15;

    private float AttackTimer = 0.0f;
    private NavMeshAgent lecturer;
    private float fieldOfViewAngle;
    private GameObject player;
    private PlayerDamage playerHealth;
    private bool playerHit = false;


    //private int layerMask;
    private Renderer rend;
    private Animation anim;

    private bool playerInSight;
    private System.Action<AttackResults> attackResultsCallback;
    private Vector3 playerLastSighting;

    public float distanceFromPlayer;

    public AttackState(ref NavMeshAgent lecturer, float fieldOfViewAngle, ref GameObject player,
        ref Renderer rend, ref bool playerInSight, PlayerDamage playerDamage, Action<AttackResults> attackResultsCallback)
    {
        this.lecturer = lecturer;
        this.fieldOfViewAngle = fieldOfViewAngle;
        this.player = player;
        //this.layerMask = layerMask;
        this.rend = rend;
        this.playerInSight = playerInSight;
        this.playerHealth = playerDamage;
        this.attackResultsCallback = attackResultsCallback;
    }

    public void OnStateEnter()
    {
        Debug.Log("Entering Attack State");
        rend.material.color = new Color32(255, 35, 35, 1);
    }

    public void OnStateExit()
    {
        Debug.Log("Exiting Attack State");
    }

    public void OnStateUpdate()
    {
        Vector3 direction = player.transform.position - lecturer.transform.position;
        float angle = Vector3.Angle(direction, lecturer.transform.forward);
        RaycastHit hit;

        if (Physics.Raycast(lecturer.transform.position, direction.normalized,
            out hit, EnemyAI.AttackDistance/*, layerMask*/))
        {
            if (hit.transform.gameObject.CompareTag("Player"))
            {
                Debug.DrawRay(lecturer.transform.position, direction.normalized * hit.distance,
                Color.red);

                playerInSight = true;
                playerLastSighting = player.transform.position;
                distanceFromPlayer = Vector3.Distance(lecturer.transform.position, player.transform.position);


                AttackTimer += Time.deltaTime;
                Debug.Log("****Hitpoints: " + playerHealth.hitpoints + "/n****Timer: " + AttackTimer);

                lecturer.destination = player.transform.position;
                if(distanceFromPlayer < 5f)
                {
                    lecturer.destination = lecturer.transform.position;

                }

                if (AttackTimer >= 2f)
                {

                    playerHealth.takeHit();
                    playerHit = true;
                    Debug.Log("****Hitpoints: " + playerHealth.hitpoints + "/n**** HIT ******" );

                    AttackTimer = 0f;
                }

                //lecturer.transform.position = Vector3.MoveTowards(lecturer.transform.position,
                //    player.transform.position, lecturer.speed * Time.deltaTime);

                //lecturer.transform.LookAt(player.transform.position);

                var attackResults = new AttackResults(playerInSight, distanceFromPlayer, playerHit);

                attackResultsCallback(attackResults);
            }
        }
        else
        {
            //playerInSight = false;

            //lecturer.transform.position = Vector3.(lecturer.transform.position,
            //    playerLastSighting, lecturer.speed * Time.deltaTime);

            lecturer.destination = playerLastSighting;

            //Debug.Log("Lecturuer: " + lecturer.transform.position + "\nPlayer: " + playerLastSighting);

            distanceFromPlayer = Vector3.Distance(lecturer.transform.position, player.transform.position);

            var attackResults = new AttackResults(playerInSight, distanceFromPlayer, playerHit);

            attackResultsCallback(attackResults);
        }
    }

    public class AttackResults
    {
        public bool playerSeen;
        public float distanceFromPlayer;
        public bool hit;

        public AttackResults(bool playerSeen, float distanceFromPlayer, bool hit)
        {
            this.playerSeen = playerSeen;
            this.distanceFromPlayer = distanceFromPlayer;
            this.hit = hit;
        }
    }
}