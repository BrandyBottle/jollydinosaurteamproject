﻿using System;
using UnityEngine;
using UnityEngine.AI;

// the attack comes only when the player is close enough to actually attack normally after the
// chase state. should enter dazed state after an attack.

public class ChaseState : MonoBehaviour, IState
{
    private NavMeshAgent lecturer;
    private float fieldOfViewAngle;
    private GameObject player;

    //private int layerMask;
    private Renderer rend;

    private bool playerInSight;
    private System.Action<ChaseResults> chaseResultsCallback;
    private Vector3 playerLastSighting;
    public float distanceFromPlayer;

    public ChaseState(ref NavMeshAgent lecturer, float fieldOfViewAngle, ref GameObject player,
        ref Renderer rend, ref bool playerInSight, Action<ChaseResults> chaseResultsCallback)
    {
        this.lecturer = lecturer;
        this.fieldOfViewAngle = fieldOfViewAngle;
        this.player = player;
        //this.layerMask = layerMask;
        this.rend = rend;
        this.playerInSight = playerInSight;
        this.chaseResultsCallback = chaseResultsCallback;
    }

    public void OnStateEnter()
    {
        lecturer.speed = 5.5f;
        Debug.Log("Entering Chase State");
        rend.material.color = new Color32(255, 150, 210, 1);

        FindObjectOfType<AudioManager>().Play("chase");
        FindObjectOfType<AudioManager>().Stop("caution_music");
        FindObjectOfType<AudioManager>().Stop("fade_in_ambience");
    }

    public void OnStateExit()
    {
        Debug.Log("Exiting Chase State");
        FindObjectOfType<AudioManager>().Stop("chase");
        FindObjectOfType<AudioManager>().Stop("caution_music");
        FindObjectOfType<AudioManager>().Play("fade_in_ambience");
    }

    public void OnStateUpdate()
    {
        Vector3 direction = player.transform.position - lecturer.transform.position;
        float angle = Vector3.Angle(direction, lecturer.transform.forward);
        RaycastHit hit = new RaycastHit();

        if (angle >= -fieldOfViewAngle && angle <= fieldOfViewAngle &&
            Physics.Raycast(lecturer.transform.position, direction.normalized,
            out hit, EnemyAI.ChaseDisance/*, layerMask*/))
        {
            if (hit.transform.gameObject.CompareTag("Player"))
            {
                lecturer.speed = 7f;
                Debug.DrawRay(lecturer.transform.position, direction.normalized * hit.distance,
                Color.red);

                playerInSight = true;
                //playerLastSighting = player.transform.position;

                //lecturer.ResetPath();
                lecturer.destination = player.transform.position;
                //lecturer.transform.position = Vector3.MoveTowards(lecturer.transform.position,
                //    player.transform.position, lecturer.speed * Time.deltaTime);
                lecturer.transform.LookAt(player.transform.position);

                if (Vector3.Distance(lecturer.transform.position, player.transform.position) <= EnemyAI.AttackDistance || Vector3.Distance(lecturer.transform.position, player.transform.position) >= EnemyAI.ChaseDisance)
                {
                    distanceFromPlayer = Vector3.Distance(lecturer.transform.position, player.transform.position);

                    var chaseResults = new ChaseResults(playerInSight, distanceFromPlayer);
                    chaseResultsCallback(chaseResults);
                }
            }
            else
            {
                lecturer.speed = 5f;
                lecturer.destination = player.transform.position;

            }
        }
        else
        {
            playerInSight = false;

            //lecturer.transform.position = Vector3.(lecturer.transform.position,
            //    playerLastSighting, lecturer.speed * Time.deltaTime);
            //lecturer.destination = playerLastSighting;
            //Debug.Log("Lecturuer: " + lecturer.transform.position + "\nPlayer: " + playerLastSighting);

            distanceFromPlayer = Vector3.Distance(lecturer.transform.position, player.transform.position);

            var chaseResults = new ChaseResults(playerInSight, distanceFromPlayer);

            chaseResultsCallback(chaseResults);
        }
    }

    public class ChaseResults
    {
        public bool playerSeen;
        public float distanceFromPlayer;

        public ChaseResults(bool playerSeen, float distanceFromPlayer)
        {
            this.playerSeen = playerSeen;
            this.distanceFromPlayer = distanceFromPlayer;
        }
    }
}