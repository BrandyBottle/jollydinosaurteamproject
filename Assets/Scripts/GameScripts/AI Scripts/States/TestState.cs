﻿//using System;
//using UnityEngine;
//using UnityEngine.AI;

//public class TestState : IState
//{
//    private Renderer rend;

//    public float fieldOfVeiwAngle;
//    public bool playerInSight;
//    public Vector3 playerLastSighting;

//    private Transform[] points;
//    private GameObject player;
//    private int layerMask;
//    private int i = 0;
//    private NavMeshAgent lecturer;
//    //public float radius = 25;
//    //private string tagToLookFor = "Player";

//    private System.Action<PatrolResults> patrolResultsCallback;
//    private NavMeshAgent navMeshAgent;

//    public TestState(ref NavMeshAgent lecturer, ref Transform[] points, float fieldOfVeiwAngle, ref GameObject player, int layerMask,
//        ref Renderer rend, ref bool playerInSight, Action<PatrolResults> patrolResultsCallback)
//    {
//        this.lecturer = lecturer;
//        this.points = points;
//        this.fieldOfVeiwAngle = fieldOfVeiwAngle;
//        this.player = player;
//        this.layerMask = layerMask;
//        this.rend = rend;
//        this.playerInSight = playerInSight;
//        this.patrolResultsCallback = patrolResultsCallback;
//    }

//    //public TestState(NavMeshAgent navMeshAgent, Transform[] points, GameObject player, ref Renderer rend, out bool playerInSight)
//    //{
//    //    this.lecturer = navMeshAgent;
//    //    this.points = points;
//    //    this.player = player;
//    //    this.rend = rend;
//    //    playerInSight = false;
//    //}

//    public void OnStateEnter()
//    {
//        rend.material.color = new Color(0.5f, 1, 1);
//        Debug.Log("Entering Patrolling State.");
//    }

//    public void OnStateExit()
//    {
//        Debug.Log("Exiting Patrolling State.");
//    }

//    public void OnStateUpdate()
//    {
//        Debug.Log(player.transform.position);
//        if (!lecturer.pathPending && lecturer.remainingDistance < 0.5f)
//        {
//            // Returns if no points have been set up
//            if (points.Length == 0)
//                return;

//            // Set the lecturer to go to the currently selected destination.
//            lecturer.destination = points[i].position;

//            // Choose the next point in the array as the destination,
//            // cycling to the start if necessary.
//            i = (i + 1) % points.Length;
//        }

//        // Bit shift the index of the layer (8) to get a bit mask
//        //make public and pass by ref
//        //int layerMask = 1 << 8;

//        // This would cast rays only against colliders in layer 8.
//        // But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
//        //layerMask = ~layerMask;

//        //playerInSight = false;

//        Vector3 direction = player.transform.position - lecturer.transform.position;
//        float angle = Vector3.Angle(direction, lecturer.transform.forward);

//        if (angle >= -60 && angle <= 60)
//        {
//            RaycastHit hit;
//            if (Physics.Raycast(lecturer.transform.position, direction.normalized,
//            out hit, 15, layerMask))
//            {
//                Debug.DrawRay(lecturer.transform.position, direction.normalized * hit.distance,
//                    Color.red);
//                Debug.Log("Sees Player");

//                playerInSight = true;
//                playerLastSighting = player.transform.position;

//                var patrolResults = new PatrolResults(playerInSight, playerLastSighting);

//                patrolResultsCallback(patrolResults);
//            }
//        }
//        else
//        {
//            Debug.DrawRay(this.lecturer.transform.position, direction.normalized * 10, Color.white);
//            Debug.DrawRay(this.lecturer.transform.position, this.lecturer.transform.TransformDirection(Vector3.forward) * 10, Color.yellow);
//        }
//    }
//}

//public class PatrolResults
//{
//    public bool playerSeen;
//    public Vector3 lastPlayerPosition;

//    public PatrolResults(bool playerSeen, Vector3 lastPlayerPosition)
//    {
//        this.playerSeen = playerSeen;
//        this.lastPlayerPosition = lastPlayerPosition;
//    }
//}

////this.lecturer.transform.position = Vector3.MoveTowards(this.lecturer.transform.position, this.points[i].position,
////    this.lecturer.speed * Time.deltaTime);

////if (Vector3.Distance(this.lecturer.transform.position, this.points[i].position) < 0.2f)
////{
////    i++;

////    if (i == this.points.Length)
////    {
////        i = 0;
////    }

////    //Debug.DrawLine(this.points[i].position, this.points[i + 1].position, Color.red);

////}

////RaycastHit hit1;
//// Does the ray intersect any objects excluding the player layer
////SpereCast
////if (Physics.Raycast(this.lecturer.transform.position, this.lecturer.transform.TransformDirection(Vector3.forward),
////    out hit1, 10, layerMask))
////{
////    Debug.DrawRay(this.lecturer.transform.position, this.lecturer.transform.TransformDirection(Vector3.forward) * hit.distance,
////            Color.red);
////    Debug.Log("Seen Player");
////}
////else
////{
////    Debug.DrawRay(this.lecturer.transform.position, this.lecturer.transform.TransformDirection(Vector3.forward) * 10, Color.white);
////    //Debug.Log("Did not Hit");
////}

////var hitObjects = Physics.OverlapSphere(this.lecturer.transform.position, radius);
////for (int i = 0; i < hitObjects.Length; i++)
////{
////    if(hitObjects[i].CompareTag(tagToLookFor))
////    {
////        Debug.Log("Sees PLayer.");
////    }
////    break;
////}