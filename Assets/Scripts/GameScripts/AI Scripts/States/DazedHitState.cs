﻿using System;
using UnityEngine;
using UnityEngine.AI;

public class DazedHitState : IState
{
    private float timer = 0.0f;
    private NavMeshAgent lecturer;
    private Action<DazedResults> dazedResultsCallback;
    private bool undazed = true;


    public DazedHitState(ref NavMeshAgent lecturer, Action<DazedResults> dazedResultsCallback)
    {
        this.lecturer = lecturer;
        this.dazedResultsCallback = dazedResultsCallback;
    }

    public void OnStateEnter()
    {
        Debug.Log("Entering Dazed State.");
        lecturer.speed = 0f;
    }

    public void OnStateExit()
    {
        Debug.Log("Exiting Dazed State.");
    }

    public void OnStateUpdate()
    {
        lecturer.destination = lecturer.transform.position;
        timer += Time.deltaTime;
        if (timer >= 5f)
        {
            undazed = true;
            var dazedresults = new DazedResults(undazed);
            dazedResultsCallback(dazedresults);
        }
    }
}
