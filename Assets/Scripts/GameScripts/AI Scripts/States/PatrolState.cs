﻿using System;
using UnityEngine;
using UnityEngine.AI;

public class PatrolState : IState
{
    //private const int AttackDistance = 5;
    //private const int ChaseDisance = 15;
    //private const int SuspicionDisance = 30;
    private Renderer rend;

    private float timer = 0.0f;

    public float fieldOfViewAngle;
    public bool playerInSight;
    public Vector3 playerLastSighting;
    public float distanceFromPlayer;

    private Transform[] points;
    private GameObject player;
    //private int layerMask;
    private int i = 0;
    private NavMeshAgent lecturer;

    private System.Action<PatrolResults> patrolResultsCallback;
    private NavMeshAgent navMeshAgent;

    public PatrolState(ref NavMeshAgent lecturer, ref Transform[] points, float fieldOfViewAngle, ref GameObject player,
        ref Renderer rend, ref bool playerInSight, Action<PatrolResults> patrolResultsCallback)
    {
        this.lecturer = lecturer;
        this.points = points;
        this.fieldOfViewAngle = fieldOfViewAngle;
        this.player = player;
        //this.layerMask = layerMask;
        this.rend = rend;
        this.playerInSight = playerInSight;
        this.patrolResultsCallback = patrolResultsCallback;
    }

    public void OnStateEnter()
    {
        lecturer.speed = 3.5f;

        rend.material.color = new Color(0.5f, 1, 1);
        Debug.Log("Entering Patrolling State.");
    }

    public void OnStateExit()
    {
        Debug.Log("Exiting Patrolling State.");
    }

    public void OnStateUpdate()
    {
        timer += Time.deltaTime;
        if (timer > 90f) // after 90 seconds start targeted patrol
        {
            var patrolResults = new PatrolResults(false, Vector3.Distance(lecturer.transform.position, player.transform.position));
            patrolResultsCallback(patrolResults);
        }

        //Debug.Log(player.transform.position);
        if (!lecturer.pathPending && lecturer.remainingDistance < 0.5f)
        {
            // Returns if no points have been set up
            if (points.Length == 0)
                return;

            // Set the lecturer to go to the currently selected destination.
            lecturer.destination = points[i].position;

            // Choose the next point in the array as the destination,
            // cycling to the start if necessary.
            i = (i + 1) % points.Length;
        }
        //layerMask = ~layerMask;
        //Debug.Log(layerMask);

        Vector3 direction = player.transform.position - lecturer.transform.position;
        float angle = Vector3.Angle(direction, lecturer.transform.forward);
        RaycastHit hit;

        if (angle >= -fieldOfViewAngle && angle <= fieldOfViewAngle &&
            Physics.Raycast(lecturer.transform.position, direction.normalized,
            out hit, EnemyAI.SuspicionDisance))
        {
            if (hit.transform.gameObject.CompareTag("Player"))
            {
                Debug.DrawRay(lecturer.transform.position,
                    direction.normalized * hit.distance, Color.red);
                Debug.Log("Sees Player");

                playerInSight = true;
                distanceFromPlayer = Vector3.Distance(lecturer.transform.position, player.transform.position);
                //playerLastSighting = player.transform.position;

                var patrolResults = new PatrolResults(playerInSight, distanceFromPlayer);

                patrolResultsCallback(patrolResults);
            }
        }
        else
        {
            Debug.DrawRay(this.lecturer.transform.position, direction.normalized * EnemyAI.SuspicionDisance, Color.yellow);
            Debug.DrawRay(this.lecturer.transform.position, direction.normalized * EnemyAI.ChaseDisance, Color.blue);
            Debug.DrawRay(this.lecturer.transform.position, direction.normalized * EnemyAI.AttackDistance, Color.red);

            Debug.DrawRay(this.lecturer.transform.position, this.lecturer.transform.TransformDirection(Vector3.forward) * EnemyAI.SuspicionDisance, Color.white);
        }
    }
}

public class PatrolResults
{
    public bool playerSeen;
    public float distanceFromPlayer;

    public PatrolResults(bool playerSeen, float distanceFromPlayer)
    {
        this.playerSeen = playerSeen;
        this.distanceFromPlayer = distanceFromPlayer;
    }
}