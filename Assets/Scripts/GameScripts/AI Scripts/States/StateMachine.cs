﻿
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    private IState currentlyRunningState;

    private IState previousState;

    public void ChangeState(IState newState)
    {
        if(this.currentlyRunningState != null)
        {
            this.currentlyRunningState.OnStateExit();
        }
        this.previousState = this.currentlyRunningState;

        this.currentlyRunningState = newState;
        this.currentlyRunningState.OnStateEnter();
    }

    public void ExecuteStateUpdate()
    {
        var runningState = this.currentlyRunningState;
        if(runningState != null)
        {
            runningState.OnStateUpdate();
        }
    }

    public void SwitchToPreviousState()
    {
        this.currentlyRunningState.OnStateExit();
        this.currentlyRunningState = this.previousState;
        this.currentlyRunningState.OnStateEnter();

    }
}
