﻿using System;
using UnityEngine;
using UnityEngine.AI;

public class DazedState : IState
{
    private float timer = 0.00f;
    private NavMeshAgent lecturer;
    private System.Action<DazedResults> dazedResultsCallback;
    private bool undazed = true;

    public DazedState(ref NavMeshAgent lecturer, Action<DazedResults> dazedResultsCallback)
    {
        this.lecturer = lecturer;
        this.dazedResultsCallback = dazedResultsCallback;
    }

    public void OnStateEnter()
    {
        Debug.Log("Entering Dazed State.");
        lecturer.speed = 0f;
    }

    public void OnStateExit()
    {
        Debug.Log("Exiting Dazed State.");
    }

    public void OnStateUpdate()
    {
        lecturer.destination = lecturer.transform.position;
        timer += Time.deltaTime;
        if (timer >= 10f)
        {
            undazed = true;
            var dazedresults = new DazedResults(undazed);
            dazedResultsCallback(dazedresults);
        }
    }
}

public class DazedResults
{
    public bool Undazed;

    public DazedResults(bool Undazed)
    {
        this.Undazed = Undazed;
    }
}