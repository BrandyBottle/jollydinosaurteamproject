﻿using System;
using UnityEngine;
using UnityEngine.AI;

internal class TargetedPatrolState : IState
{
    private NavMeshAgent lecturer;
    private Transform[] points;
    private GameObject player;
    private Renderer rend;

    float timer = 0.00f;
    public float fieldOfViewAngle;
    public bool playerInSight;
    public Vector3 playerLastSighting;
    public float distanceFromPlayer;
    private Transform PointClosestToPlayer;
    private double DistancefromPoint;
    private double pointTemp;
    private int placeInArray = 0;


    private System.Action<PatrolResults> targetResultsCallback;



    public TargetedPatrolState(ref NavMeshAgent lecturer, ref Transform[] points, float fieldOfViewAngle, ref GameObject player,
        ref Renderer rend, ref bool playerInSight, Action<PatrolResults> targetResultsCallback)
    {
        this.lecturer = lecturer;
        this.points = points;
        this.player = player;
        this.rend = rend;
        this.playerInSight = playerInSight;
        this.fieldOfViewAngle = fieldOfViewAngle;
        this.targetResultsCallback = targetResultsCallback;

    }
    public void OnStateEnter()
    {
        lecturer.speed = 4f;

        PointClosestToPlayer = points[0];
        DistancefromPoint = Vector3.Distance(points[0].transform.position, player.transform.position);

        for (int i = 0; i < points.Length - 1; i++)
        {
            pointTemp = Vector3.Distance(points[i].transform.position, player.transform.position);
            //Debug.Log("pointTemp: " + pointTemp + "\nI: " + i);

            if (pointTemp < DistancefromPoint)
            {
                DistancefromPoint = pointTemp;
                PointClosestToPlayer = points[i];
                placeInArray = i;
                //Debug.Log("DistancefromPoint: " + DistancefromPoint + "\nI: " + i);
            }
        }

        lecturer.destination = PointClosestToPlayer.position;



        rend.material.color = new Color(0.9f, 0.5f, 0.7f);
        Debug.Log("Entering Target Patrolling State.");
    }

    public void OnStateExit()
    {
        Debug.Log("Exiting Target Patrolling State.");
    }

    public void OnStateUpdate()
    {
        timer += Time.deltaTime;
        if (timer >= 40f)
        {
            Debug.Log("\nTimers Up*****\n*****\n*******\n");
            DistancefromPoint = Vector3.Distance(points[0].transform.position, player.transform.position);
            for (int i = 0; i < points.Length - 1; i++)
            {
                pointTemp = Vector3.Distance(points[i].transform.position, player.transform.position);
                //Debug.Log("pointTemp: " + pointTemp + "\nI: " + i);

                if (pointTemp < DistancefromPoint)
                {
                    DistancefromPoint = pointTemp;
                    PointClosestToPlayer = points[i];
                    placeInArray = i;
                    //Debug.Log("DistancefromPoint: " + DistancefromPoint + "\nI: " + i);
                }
            }
            lecturer.destination = PointClosestToPlayer.position;

            timer = 0.0f;
        }
        else if (lecturer.remainingDistance < 2f)
        {

            lecturer.destination = points[placeInArray + 1].position;

            int temp = placeInArray + 1;
            // Choose the next point in the array as the destination,
            // cycling to the start if necessary.
            temp = (temp + 1) % points.Length;

            Debug.Log("\n" + timer + "\n *****\n*******\n****");
        }


        Vector3 direction = player.transform.position - lecturer.transform.position;
        float angle = Vector3.Angle(direction, lecturer.transform.forward);
        RaycastHit hit;

        if (angle >= -fieldOfViewAngle && angle <= fieldOfViewAngle &&
            Physics.Raycast(lecturer.transform.position, direction.normalized,
            out hit, EnemyAI.SuspicionDisance))
        {
            if (hit.transform.gameObject.CompareTag("Player"))
            {
                Debug.DrawRay(lecturer.transform.position,
                    direction.normalized * hit.distance, Color.red);
                Debug.Log("Sees Player");

                playerInSight = true;
                distanceFromPlayer = Vector3.Distance(lecturer.transform.position, player.transform.position);
                //playerLastSighting = player.transform.position;

                var patrolResults = new PatrolResults(playerInSight, distanceFromPlayer);

                targetResultsCallback(patrolResults);
            }
        }
        else
        {
            Debug.DrawRay(this.lecturer.transform.position, direction.normalized * EnemyAI.SuspicionDisance, Color.yellow);
            Debug.DrawRay(this.lecturer.transform.position, direction.normalized * EnemyAI.ChaseDisance, Color.blue);
            Debug.DrawRay(this.lecturer.transform.position, direction.normalized * EnemyAI.AttackDistance, Color.red);

            Debug.DrawRay(this.lecturer.transform.position, this.lecturer.transform.TransformDirection(Vector3.forward) * EnemyAI.SuspicionDisance, Color.white);
        }
    }
}
