﻿using System;
using UnityEngine;
using UnityEngine.AI;

public class ItemSuspicionState : IState
{
    private float timer = 0.0f;
    public float speed = 72f;
    private NavMeshAgent lecturer;
    private float fieldOfVeiwAngle;
    private GameObject player;
    private bool playerInSight;
    private float distanceFromPlayer;
    private Action<SuspicionState.SuspicionResults> suspicionResultsCallback;
    private Vector3 playerLastSighting;
    private Vector3 itemPosition;



    public ItemSuspicionState(ref NavMeshAgent navMeshAgent, float fieldOfVeiwAngle, ref GameObject player, ref bool playerInSight, float distanceFromPlayer, Vector3 itemPos, Action<SuspicionState.SuspicionResults> playerLosted)
    {
        this.lecturer = navMeshAgent;
        this.fieldOfVeiwAngle = fieldOfVeiwAngle;
        this.player = player;
        this.playerInSight = playerInSight;
        this.distanceFromPlayer = distanceFromPlayer;
        this.itemPosition = itemPos;
        this.suspicionResultsCallback = playerLosted;
    }

    public void OnStateEnter()
    {
        Debug.Log("Entering ITEM Suspiicion State");

        lecturer.speed = 5f;

    }

    public void OnStateUpdate()
    {
        Debug.Log("Entering ITEM 111 Suspiicion State");

        Vector3 direction = player.transform.position - lecturer.transform.position;
        float angle = Vector3.Angle(direction, lecturer.transform.forward);
        RaycastHit hit;

        // get AI to go the the direction of what they seen.
        if (angle >= -fieldOfVeiwAngle && angle <= fieldOfVeiwAngle &&
            Physics.Raycast(lecturer.transform.position, direction.normalized,
            out hit, EnemyAI.SuspicionDisance) && hit.transform.gameObject.CompareTag("Player"))
        {
            if (hit.transform.gameObject.CompareTag("Player"))
            {
                Debug.DrawRay(lecturer.transform.position, direction.normalized * hit.distance,
                Color.red);

                playerLastSighting = player.transform.position;

                lecturer.destination = playerLastSighting;

                if (Vector3.Distance(lecturer.transform.position, player.transform.position) <= EnemyAI.ChaseDisance)
                {
                    distanceFromPlayer = Vector3.Distance(lecturer.transform.position, player.transform.position);

                    var suspicionResults = new SuspicionState.SuspicionResults(playerInSight, distanceFromPlayer);
                    suspicionResultsCallback(suspicionResults);
                }
            }
        }
        else
        {
            Debug.Log("Entering ITEM 2222 Suspiicion State");

            lecturer.destination = itemPosition;

            if (lecturer.remainingDistance < 5f)
            {
                timer += Time.deltaTime;

                //Should be a look around animation
                if (timer <= 5f)
                {
                    lecturer.transform.Rotate(Vector3.up, speed * Time.deltaTime);

                    //lecturer.transform.LookAt(player.transform.position);
                    //Debug.Log(timer + " Time dt" + Time.deltaTime);

                }
                else
                {
                    lecturer.transform.LookAt(Vector3.forward);
                    Debug.Log("forward");
                    playerInSight = false;
                    distanceFromPlayer = Vector3.Distance(lecturer.transform.position, player.transform.position);

                    var suspicionResults = new SuspicionState.SuspicionResults(playerInSight, distanceFromPlayer);

                    suspicionResultsCallback(suspicionResults);
                }
            }
        }

    }

    public void OnStateExit()
    {
        Debug.Log("Exiting ITEM Suspiicion State");

    }
}