﻿using UnityEngine;
using UnityEngine.AI;

internal class BasementChaseState : IState
{
    private NavMeshAgent navMeshAgent;
    private Transform[] points;
    private GameObject player;
    private int i = 0;

    public BasementChaseState(ref NavMeshAgent navMeshAgent, ref Transform[] points, ref GameObject player)
    {
        this.navMeshAgent = navMeshAgent;
        this.points = points;
        this.player = player;
    }

    public void OnStateEnter()
    {
        navMeshAgent.speed = 4.5f;
    }

    public void OnStateExit()
    {
        
    }

    public void OnStateUpdate()
    {
        if (navMeshAgent.remainingDistance < 2f)
        {
            // Returns if no points have been set up
            if (points.Length == 0)
                return;

            // Set the lecturer to go to the currently selected destination.
            navMeshAgent.destination = points[i].position;

            // Choose the next point in the array as the destination,
            // cycling to the start if necessary.
            i = (i + 1) % points.Length;
        }

        Collider[] hitColliders = Physics.OverlapSphere(navMeshAgent.transform.position, 2f);
        foreach (Collider hit in hitColliders)
        {
            if (hit.gameObject.tag == "Player")
            {
                navMeshAgent.speed = 0f;
                Debug.Log("***EndGame *** Player Caught!!");

            }
        }
    }
}