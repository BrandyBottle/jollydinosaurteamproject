﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;



[System.Serializable]

public class ThrowableObject : InteractableObject {

    public EnemyAI EnemyAI;
    bool thrown = false;
    public Transform playerPosition;
    public Transform parentCamera;
    public float soundRange = 50f;
    AudioSource collisionSound;

	// Use this for initialization
	void Start()
    {
        collisionSound = GetComponent<AudioSource>();
        playerPosition = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        parentCamera = GameObject.FindObjectOfType<Camera>().GetComponent<Transform>();
        uiText = GameObject.Find("InteractableText").GetComponent<Text>();
        uiImage = GameObject.Find("Crosshair").GetComponent<Image>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (holding)
        {
            holdObject();

            // Right Click
            if (Input.GetMouseButtonDown(1))
            {
                throwObject();
                thrown = true;
            }
        }
    }

    void OnMouseDown()
    {
        float distance = Vector3.Distance(this.transform.position, playerPosition.transform.position);
        if (distance <=  2.55f && !holding && triggered)
        {
            holding = true;

            //this.uiText.enabled = false;
            this.uiImage.enabled = false;
            this.GetComponent<Rigidbody>().useGravity = false;
            this.GetComponent<Rigidbody>().detectCollisions = true;
            this.GetComponent<Rigidbody>().isKinematic = true;
        }
    }

    void holdObject()
    {
        this.GetComponent<Rigidbody>().velocity = Vector3.zero;
        this.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        this.transform.parent = null;
        //Debug.Log("HOLDING");
        this.transform.SetParent(parentCamera);
    }

    void throwObject()
    {
        this.GetComponent<Rigidbody>().isKinematic = false;
        this.GetComponent<Rigidbody>().AddForce(parentCamera.forward * 600);
        this.GetComponent<Rigidbody>().useGravity = true;
        this.transform.SetParent(null);
        holding = false;

        //this.uiText.enabled = true;
        this.uiImage.enabled = true;
    }

    void OnCollisionEnter(Collision col)
    {
        Debug.Log("********** 11 *************");
        
        if (thrown)
        {

            if (col.gameObject.tag == "Floor")
            {

                collisionSound.Play();
                checkIfAiHeard();

                thrown = false;
            }
            else if (col.gameObject.tag == "Lecturer")
            {
                Debug.Log("********** 3 *************");

                collisionSound.Play();
                EnemyAI.AIItemHit();
                thrown = false;
            }
        }
    }

    void checkIfAiHeard()
    {
        bool inRange;
        // Gathers every object the OverlapSphere has collided with
        Collider[] objectsInRange = Physics.OverlapSphere(this.transform.position, soundRange);
        foreach (Collider objectInRange in objectsInRange)
        {
            // Checks one object at a time and checks if it's the lecturer
            if (objectInRange.gameObject.tag == "Lecturer")
            {
                Debug.Log("In range!");
                inRange = true;
                Debug.Log("********** 4 *************");

                SoundResults soundResults = new SoundResults(objectInRange.transform.position, inRange);

                EnemyAI.ItemInRange(soundResults);

            }
        }
    }
}
