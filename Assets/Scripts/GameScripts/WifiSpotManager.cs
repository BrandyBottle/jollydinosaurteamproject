﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

using UnityEngine;

public class WifiSpotManager : MonoBehaviour {

    public delegate void StateChange(int state);
    public static event StateChange stateChange;

    [SerializeField] private GameObject mesh1;
    [SerializeField] private GameObject mesh2;
    [SerializeField] private GameObject mesh3;
    [SerializeField] private GameObject mesh4;
    [SerializeField] private GameObject mesh5;

    WifiSpot[] wifiSpots;
    WifiSpot currentActiveWifiSpot;

    public int currentSpot;
    public Text distanceText;
    Transform playerPosition;
    bool downloading;
    bool stateChanged = false;
    float timer;

    

    // Better way to do this in future
    int combination1 = 1;
    int combination2 = 6;
    int combination3 = 9;

    int triggers = 0;

    float distance;

	// Use this for initialization
	void Start () {
        playerPosition = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();

        LevelStateManager.onStartWifiManager += activateFirstSpot;

        wifiSpots = FindObjectsOfType<WifiSpot>();
        currentSpot = wifiSpots.Length;

        mesh1.GetComponent<Renderer>().enabled = false;
    }
	
	// Update is called once per frame
	void Update ()
    {
        timer += Time.deltaTime;
        if(timer > 10f)
        {
            distanceText.text = "";
        }
        if (currentActiveWifiSpot.triggered && !downloading)
        {
            downloading = true;
            timer = 0f;
        }

        // Output Downloading Text
        if(downloading)
        {
            if(triggers < 2)
            {
                if (timer < 5f)
                {
                    distanceText.text = "Fetching Password";
                    //FindObjectOfType<AudioManager>().Play("downloading");
                }
                else if(timer > 5f)
                {
                    distanceText.text = "Wifi Connection Lost Try Again";
                    spotTriggered();
                }
            }
            else
            {
                if (timer < 5f)
                {
                    distanceText.text = "Fetching Password";
                }
                else if (timer > 5f)
                {
                    distanceText.text = "Password Retrieved!";
                    
                    spotTriggered();
                }
            }
            
        }
        calculateDistance();
        //distanceText.text = "Distance: " + (int)distance;

        if(triggers == 3 && !stateChanged)
        {
            // Next State
            stateChange(2);
            stateChanged = true;
            FindObjectOfType<LockerStateTrigger>().getCombination();
        }
        
        if((int)distance <= 5)
        {
            mesh1.GetComponent<Renderer>().enabled = true;
            mesh2.GetComponent<Renderer>().enabled = false;
            mesh3.GetComponent<Renderer>().enabled = false;
            mesh4.GetComponent<Renderer>().enabled = false;
            mesh5.GetComponent<Renderer>().enabled = false;
        }

        else if ((int)distance <= 50)
        {
            mesh2.GetComponent<Renderer>().enabled = true;
            mesh1.GetComponent<Renderer>().enabled = false;
            mesh3.GetComponent<Renderer>().enabled = false;
            mesh4.GetComponent<Renderer>().enabled = false;
            mesh5.GetComponent<Renderer>().enabled = false;
        }

        else if ((int)distance <= 100)
        {
            mesh3.GetComponent<Renderer>().enabled = true;
            mesh1.GetComponent<Renderer>().enabled = false;
            mesh2.GetComponent<Renderer>().enabled = false;
            mesh4.GetComponent<Renderer>().enabled = false;
            mesh5.GetComponent<Renderer>().enabled = false;
        }

        
        else if((int)distance > 101)
        {
            mesh4.GetComponent<Renderer>().enabled = true;
            mesh1.GetComponent<Renderer>().enabled = false;
            mesh2.GetComponent<Renderer>().enabled = false;
            mesh3.GetComponent<Renderer>().enabled = false;
            mesh5.GetComponent<Renderer>().enabled = false;
        }
	}

    // Randomly selects one of the available Wifi Spots and activates it
    void activateFirstSpot()
    {
        int numberOfSpots = wifiSpots.Length;

        int selectedWifiSpot = Random.Range(0, numberOfSpots);

        currentActiveWifiSpot = wifiSpots[selectedWifiSpot];
        currentSpot = selectedWifiSpot;

        currentActiveWifiSpot.activate();
    }

    void activateNewSpot()
    {
        int numberOfSpots = wifiSpots.Length;
        int selectedWifiSpot = 0;
        WifiSpot tempWifiSpot = currentActiveWifiSpot;

        // Prevents the same spot being activated again in succession
        while (currentActiveWifiSpot == tempWifiSpot)
        {
            selectedWifiSpot = Random.Range(0, numberOfSpots);
            tempWifiSpot = wifiSpots[selectedWifiSpot];
        }

        currentActiveWifiSpot = wifiSpots[selectedWifiSpot];

        currentActiveWifiSpot.activate();
    }


    // When a Wifi Spot is triggered, set a new one.
    public void spotTriggered()
    {
        triggers++;
        currentActiveWifiSpot.deactivate();
        downloading = false;

        // Don't activate a new spot if there's already been 3 connection attempts
        if(triggers < 3)
        {
            activateNewSpot();
        }
        
    }

    void calculateDistance()
    {
        distance = Vector3.Distance(currentActiveWifiSpot.transform.position, playerPosition.position);
    }
}
