﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class toilet_voice : MonoBehaviour
{
    public bool alreadyPlayed = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider collider)
    {


        if (collider.tag == "Player" && alreadyPlayed == false)
        {
            alreadyPlayed = true;
            FindObjectOfType<AudioManager>().Play("toilet");
        }
    }
}
