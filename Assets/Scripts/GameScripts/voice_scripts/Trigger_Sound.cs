﻿using UnityEngine.Audio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger_Sound : MonoBehaviour
{
    /*
    public AudioClip SoundToPlay;
    public float volume;
    AudioSource audio;*/
    public bool alreadyPlayed = false;

    // Start is called before the first frame update
    void Start()
    {
        //audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /*
    void OnTriggerEnter()
    {
        if (!alreadyPlayed)
        {
            audio.PlayOneShot(SoundToPlay, volume);
            alreadyPlayed = true;
        }

    }*/
    
    void OnTriggerEnter(Collider collider)
    {
        if(collider.tag == "Player" && alreadyPlayed == false)
        {
            FindObjectOfType<AudioManager>().Play("map");
            alreadyPlayed = true;
        }   
    }
}
