﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class locker_voice : MonoBehaviour
{
    public bool alreadyPlayed = false;
    public bool alreadyPlayed1 = false;

    [SerializeField] AudioClip locker;
    [SerializeField] AudioClip locker1;

    AudioSource f_AudioSource;

    // Start is called before the first frame update
    void Start()
    {
        f_AudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider collider)
    {


        if (collider.tag == "Player" && alreadyPlayed == false)
        {
            alreadyPlayed = true;
            f_AudioSource.clip = locker;
            f_AudioSource.Play();
            //FindObjectOfType<AudioManager>().Play("locker");
        }

        else if (collider.tag == "Player" && alreadyPlayed1 == false)
        {
            f_AudioSource.clip = locker1;
            f_AudioSource.Play();
            //FindObjectOfType<AudioManager>().Stop("locker");
            alreadyPlayed1 = true;
            //FindObjectOfType<AudioManager>().Play("locker2");
        }
    }
}
