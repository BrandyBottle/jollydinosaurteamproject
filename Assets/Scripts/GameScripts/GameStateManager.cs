﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStateManager : MonoBehaviour {

    private bool gameWon = false;
    private bool gameLost = false;

	//public Text uiText;

    void Start()
    {
        // WIP, subscribe to other event listeners... Win state etc....
        PlayerDamage.onLose += loseState;
    }

    void Update()
    {
        // Pause the Game
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(Time.timeScale == 1)
            {
                Time.timeScale = 0;
            }
            else
            {
                Time.timeScale = 1;
            }
        }
        if(gameLost)
        {
            SceneManager.LoadScene(6);
        }
    }

    public void winState()
    {
        if(!gameWon)
        {
            gameWon = true;
			//uiText.text = "Collected CA and Escaped!";
			//uiText.enabled = true;
			//Debug.Log(uiText.text);
        }
    }

    public void loseState()
    {
        if (!gameLost)
        {
            gameLost = true;
            // Go to Game Over Scene
        }
    }
}
