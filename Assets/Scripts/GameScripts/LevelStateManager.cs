﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class LevelStateManager : MonoBehaviour
{
    public delegate void LevelAction();
    public static event LevelAction onStartWifiManager;

    public enum States {FindUSBKey, WifiFinder, GetUSBKey, UploadCA, Escape};

    float timer;
    States currentState = States.FindUSBKey;
    bool showText = true;
    bool createdMessage = false;
    public MessageBubbleManager textBubble;
    public Text currentObjective;
    float degree = 0, steps = 0;
    float degree2 = 10;
    public Animator anim,anim2, anim3, anim4, anim5, anim6;


    // Start is called before the first frame update
    void Start()
    {
        Camera cam = GameObject.Find("Camera").GetComponent<Camera>();
        Camera cam2 = GameObject.Find("Camera2").GetComponent<Camera>();
        cam.enabled = true;
        cam2.enabled = false;
        LockerStateTrigger.stateChange += changeState;
        WifiSpotManager.stateChange += changeState;
        ComputerTrigger.stateChange += changeState;
      
        anim= GameObject.Find("Camera2").GetComponent<Animator>();
        anim2= GameObject.Find("Point Light (flicker5)").GetComponent<Animator>();
        anim3 = GameObject.Find("Point Light (flicker7)").GetComponent<Animator>();
        anim4 = GameObject.Find("Point Light (flicker10)").GetComponent<Animator>();
        anim5 = GameObject.Find("Point Light (flicker11)").GetComponent<Animator>();
        anim6 = GameObject.Find("LockerDoor").GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        switch((int)currentState)
        {
            case 0:
                FindUSBKeyState();
                break;

            case 1:
                WifiFinderState();
                break;

            case 2:
                GetUSBKeyState();
                break;

            case 3:
                UploadCAState();
                break;

            case 4:
                EscapeState();
                break;
        }
    }

    void FindUSBKeyState()
    { 
        if(!createdMessage)
        {
            /*string[] messages = { "Here Enda is on the hunt for anyone submitting late",
                                  "He isn’t impressed with all the late submissions and lack of attendance recently.",
                                  "I left your usb stick in your locker. So if you don’t want a 45 minute lecture from the man, I’d run to the locker room and grab it." };
            textBubble.createMessage(messages, 2.5f);*/
            currentObjective.text = "Retrieve USB from Locker Room";
            createdMessage = true;
        }
        
    }

    void WifiFinderState()
    {
        GameObject Player = GameObject.FindGameObjectWithTag("Player");
        Camera cam = GameObject.Find("Camera").GetComponent<Camera>();
        Camera cam2 = GameObject.Find("Camera2").GetComponent<Camera>();
        cam.enabled=false;
        cam2.enabled = true;
        anim.Play("LockerRoom Lights");
        ObjectRotation(Player);
        
        if (!createdMessage)
        {
            string[] messages = { "Here, forgot to mention they’re after resetting all locker passwords.",
                                  "You’re gonna have to log on and get your new password. Wifi is dodgy in that hole though. Just look for some place in the College with strong Wifi. ",
                                  "Should be grand." };
            textBubble.createMessage(messages, 2.5f);
            currentObjective.text = "Fill the Wifi Strength on your phone to connect and retrieve password";
            createdMessage = true;
            
        }

    }
    public void ObjectRotation(GameObject Player)
    {
        Camera cam = GameObject.Find("Camera").GetComponent<Camera>();
        Camera cam2 = GameObject.Find("Camera2").GetComponent<Camera>();
        if (degree<=240)
        {
            Player.transform.Rotate(0, degree, 0);
            degree = degree + 1;
            if(degree==125)
            {
                FindObjectOfType<AudioManager>().Play("LightsOut");
                GameObject light1 = GameObject.FindGameObjectWithTag("LightOne");
                light1.SetActive(false);
                
            }
           else if (degree == 140)
            {
                FindObjectOfType<AudioManager>().Play("LightsOut");
                GameObject light2 = GameObject.FindGameObjectWithTag("LightTwo");
                light2.SetActive(false);

            }
            else if (degree == 155)
            {
                FindObjectOfType<AudioManager>().Play("LightsOut");
                GameObject light3= GameObject.FindGameObjectWithTag("LightThree");
                light3.SetActive(false);
            }
            else if (degree == 180)
            {
                FindObjectOfType<AudioManager>().Play("LightsOut");
                GameObject light4 = GameObject.FindGameObjectWithTag("LightFour");
                light4.SetActive(false);
            }
            else if (degree == 240)
            {
                FindObjectOfType<AudioManager>().Play("LightsOut");
                GameObject light5 = GameObject.FindGameObjectWithTag("LightFive");
                light5.SetActive(false);


            }
            StartCoroutine(ExecuteAfterTime(3));
            
        }
        else
        {
            Player.transform.Rotate(0, 240, 0);
            //     ObjectMove(Player);
            cam.enabled = true;
            cam2.enabled = false;
                anim2.Play("FlickeringLight");
                anim3.Play("FlickeringLight");
                anim4.Play("FlickeringLight");
                anim5.Play("FlickeringLight");

        }
  
    }
    IEnumerator ExecuteAfterTime(float time)
    {
        yield return new WaitForSeconds(time);

        // Code to execute after the delay
        FindObjectOfType<AudioManager>().Play("Pockets");

        StartCoroutine(ExecuteAfterTime2(5));
    }
    IEnumerator ExecuteAfterTime2(float time)
    {
        yield return new WaitForSeconds(time);
        GameObject flashlight = GameObject.FindGameObjectWithTag("Flashlight");
        flashlight.GetComponent<Light>().enabled = true;
        
    }

        void GetUSBKeyState()
    {
        if (!createdMessage)
        {
            string[] messages = { "What’s the story get your stick from your locker yet?"};
            textBubble.createMessage(messages, 2.5f);
            currentObjective.text = "Open your Locker and retrieve USB";
            createdMessage = true;
        }
    }

    void UploadCAState()
    {
        anim6.Play("LockerOpening");
        if (!createdMessage)
        {
            string[] messages = { "Most of the computers are knackered. Technicians have been saying they’ll fix them for like 2 weeks now.",
                                  "I’d try to find a working machine and upload that assignment there. They get great Wifi speeds on them ancient machines.",
                                  "Downloaded the entirety of Bruces Price is Right there today during a lecture."};
            textBubble.createMessage(messages, 2.5f);
            currentObjective.text = "Find a working PC to upload CA";
            createdMessage = true;
        }
    }

    void EscapeState()
    {
        if (!createdMessage)
        {
            string[] messages = { "When you’re done there, meet me outside.”" };
            textBubble.createMessage(messages, 2.5f);
            currentObjective.text = "Escape";
            createdMessage = true;
        }
    }


    public void changeState(States state)
    {
        currentState = state;
        if((int)currentState == 1)
        {
            onStartWifiManager();
        }
        createdMessage = false;
        timer = 0f;
    }

    public void changeState(int state)
    {
        currentState = (States)state;
        if ((int)currentState == 1)
        {
            onStartWifiManager();
        }
        createdMessage = false;
        timer = 0f;
    }

    // Add a button that triggers this and it'll display the current level objective
    void seeCurrentObjective()
    {
        if(showText == false)
        {
            timer = 0f;
            createdMessage = false;
        }
        
    }

    public int getCurrentState()
    {
        return (int)currentState;
    }
}
