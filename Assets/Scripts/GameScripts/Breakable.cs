﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breakable : MonoBehaviour
{
    public float speed;
    public Transform target;
    public bool raised = false;
    private float timeLeft = 0.0f;
    public Transform wall;

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            LowerWall();
        }
    }
    private void OnTriggerStay(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            LowerWall();
        }
    }

    private void LowerWall()
    {

        if (Vector3.Distance(transform.position, target.position) > 0.5f)
        {
            float step = speed * Time.deltaTime;

            //transform.Translate (0,Time.deltaTime,0,Space.World);
            transform.position = Vector3.MoveTowards(transform.position, target.position, step);

        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
