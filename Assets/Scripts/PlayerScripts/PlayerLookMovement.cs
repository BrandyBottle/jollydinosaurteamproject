﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class PlayerLookMovement : MonoBehaviour
{

    [SerializeField] string mouseX, mouseY;
    [SerializeField] float sensitivity;
    [SerializeField] Transform playerTransform;

    // A variable to prevent you from rotating 360 degrees.
    float xClamp;

    void Start()
    {
        lockCursor();
        xClamp = 0.0f;
    }

    // Update is called once per frame
    void Update ()
    {
        RotateCamera();
	}

    // Prevents Mouse Cursor from moving out of window
    void lockCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    void RotateCamera()
    {
        float cameraX = Input.GetAxis(mouseX) * sensitivity * Time.deltaTime;
        float cameraY = Input.GetAxis(mouseY) * sensitivity * Time.deltaTime;

        cameraY = cameraClamp(cameraY);

        transform.Rotate(Vector3.left * cameraY);
        playerTransform.Rotate(Vector3.up * cameraX);
    }

    float cameraClamp(float cameraY)
    {
        xClamp += cameraY;

        if (xClamp > 90.0f)
        {
            xClamp = 90.0f;
            return 0.0f;

        }

        else if (xClamp < -90.0f)
        {
            xClamp = -90.0f;
            return 0.0f;
        }

        return cameraY;
    }
}
