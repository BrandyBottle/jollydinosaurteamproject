﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour {

    public float shakeIntensity;
    public bool shouldShake;
    Transform gameCamera;

    Vector3 startPosition;

	// Use this for initialization
	void Start () {

        gameCamera = this.transform;
        startPosition = gameCamera.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
		if(shouldShake)
        {
            gameCamera.localPosition = startPosition + Random.insideUnitSphere * shakeIntensity;
        }
	}

    void stopShake()
    {
        shouldShake = false;
    }


}
