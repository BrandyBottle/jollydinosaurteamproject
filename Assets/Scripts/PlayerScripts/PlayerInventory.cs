﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;


// WORK IN PROGRESS
public class PlayerInventory : MonoBehaviour
{

    // Use Enums in future
    // 0 == Unarmed
    // 1 == Coffee

    // 2 == Health

    public FirstPersonController controller;
    public PlayerDamage health;

    public bool hasKey = false;
    public Text inventoryText;
    [SerializeField] public Image coffeeIcon;
    [SerializeField] public Image fistIcon;

    public MessageBubbleManager bubble;

    int current;
    public Dictionary<int, int> inventory = new Dictionary<int, int>();

	// Use this for initialization
	void Start ()
    {
        inventory.Add(0, 0); // Default Unarmed
        inventory.Add(1, 3); // Coffee

        current = 0;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.M))
        { 
            nextItem();
        }
        if (Input.GetKeyDown(KeyCode.N)) 
        {
            previousItem();
        }

        // Right click to use item
        if(Input.GetMouseButtonDown(1))
        {
            useItem();
        }
        setText();
    }

    public void getKey()
    {
        hasKey = !hasKey;
    }

    public void pickUp(int id)
    {
        storableItem(id);
        /*if(storableItem(id))
        {
            storeItem(id);
        }
        else
        {
            consumeItem(id);
        }*/
    }

    void storableItem(int id)
    {
        switch(id)
        {
            // coffee
            case 1:
                storeItem(id);
                break;

            default:
                consumeItem(id);
                break;
        }
    }

    void storeItem(int id)
    {
        inventory[id] += 1;
    }

    // For all the items that are automatically used on collection
    void consumeItem(int id)
    {
        switch(id)
        {
            // health
            case 2:
                health.gainHealth();
                break;

            default:
                break;
        }
    }

    void useItem()
    {
        // Only if they have at least one of the item
        if(inventory[current] > 0)
        {
            inventory[current] -= 1;
            switch(current)
            {
                // Coffee, speed the player up
                case 1:
                    controller.PowerUp();
                    string[] messages = { "Health Tracker Notification: That's given you quite the SPEED BOOST! Remember it's only temporary!" };
                    bubble.createMessage(messages, 2.5f);
                    FindObjectOfType<AudioManager>().Play("slurp");
                    break;

                default:
                    break;
            }
            if(inventory[current] <= 0)
            {
                current = 0;
            }
        }
        
    }

    void nextItem()
    {
        current = current + 1;

        if(current > 1)
        {
            current = 0;
        }

        setText();
    }

    void previousItem()
    {
        current = current - 1;

        if(current < 0)
        {
            current = 1;
        }

        setText();
    }

    void setText()
    {
        string text = "";
        var amount = inventory[current];

        if(amount <= 0)
        {
            current = 0;
        }

        switch(current)
        {
            case 1:
                text = "Coffee x" + amount;
                coffeeIcon.enabled = true;
                fistIcon.enabled = false;
                break;

            default:
                text = "Unarmed";
                coffeeIcon.enabled = false;
                fistIcon.enabled = true;
                break;
        }
        inventoryText.text = text; 
    }
}
