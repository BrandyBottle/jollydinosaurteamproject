﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDamage : MonoBehaviour {

    public int hitpoints = 2;

    public delegate void LoseAction();
    public static event LoseAction onLose;

    public MessageBubbleManager bubble;

	// Use this for initialization
	void Start ()
    {
        bubble = GameObject.Find("MessageBubbleManager").GetComponent<MessageBubbleManager>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(hitpoints <= 0)
        {
            onLose();
        }
		
	}

    public void takeHit()
    {
        string[] messages = { "Health Tracker Notification: You can't take too many more of those! Get out if there!" };
        bubble.createMessage(messages, 2.5f);
        hitpoints--;
    }

    public void gainHealth()
    {
        if(hitpoints < 2)
        {
            hitpoints++;
            string[] messages = { "Health Tracker Notification: Looking healthy my man! Look FULL OF LIFE!" };
            bubble.createMessage(messages, 2.5f);
        }
        else
        {
            string[] messages = { "Health Tracker Notification: You're perfectly healthy. Why you gotta be a glutton?" };
            bubble.createMessage(messages, 2.5f);
        }
    }
}
