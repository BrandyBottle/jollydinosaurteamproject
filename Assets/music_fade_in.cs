﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class music_fade_in : MonoBehaviour
{
    public LevelStateManager levelStateManager;

    void OnTriggerEnter(Collider collider)
    {
        if (levelStateManager.getCurrentState() == 3)
        {

            if (collider.gameObject.tag == "Player")
            {
                //FindObjectOfType<AudioManager>().Play("escape_intro");

                //StartCoroutine(delay());

                FindObjectOfType<AudioManager>().Play("upload_area");

                FindObjectOfType<AudioManager>().Stop("fade_in_ambience");

            }
        }

        if (levelStateManager.getCurrentState() == 0)
        {

            if (collider.gameObject.tag == "Player")
            {
                
                //FindObjectOfType<AudioManager>().FadeIn("fade_in_ambience", 0.8f);
                
            }
        }

        
    }

    
    void OnTriggerExit(Collider collider)
    {
        if (levelStateManager.getCurrentState() == 4)
        {

            if (collider.gameObject.tag == "Player")
            {
                //FindObjectOfType<AudioManager>().Play("escape_intro");

                //StartCoroutine(delay());

                FindObjectOfType<AudioManager>().Stop("fade_in_ambience");
                FindObjectOfType<AudioManager>().Play("escape");
            } 
        }

       
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator delay()
    {
        yield return new WaitForSeconds(1);
        
    }
}
