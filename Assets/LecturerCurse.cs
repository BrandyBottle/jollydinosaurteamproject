﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LecturerCurse : MonoBehaviour
{

    public Animation anim;
    public RectTransform speechBubble;
    float timer = 0;

    // Update is called once per frame
    void Update()
    {
        if(anim.IsPlaying("Walking"))
        {
            Debug.Log("HELLO THERE GOVENOR");
            speechBubble.gameObject.SetActive(true);
            if(timer < .5)
            {
                speechBubble.localPosition = new Vector3(-2.88f, 2.06f, 0f);
            }
            else if(timer < 1)
            {
                speechBubble.localPosition = new Vector3(2.88f, 2.06f, 0f);
            }
            else
            {
                timer = 0;
            }
            timer += Time.deltaTime;
        }
        else
        {
            speechBubble.gameObject.SetActive(false);
        }
    }
}
