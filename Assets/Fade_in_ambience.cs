﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fade_in_ambience : MonoBehaviour
{
    public bool alreadyPlayed = false;
    public LevelStateManager levelStateManager;

    void OnTriggerEnter(Collider collider)
    {

        if(levelStateManager.getCurrentState() == 1)
        {
            if (collider.gameObject.tag == "Player" && alreadyPlayed == false)
            {
                alreadyPlayed = true;
                FindObjectOfType<AudioManager>().Stop("fade_in_ambience");
                FindObjectOfType<AudioManager>().Play("fade_in_ambience");

            }
        }

        
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
