﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ComputerTrigger : MonoBehaviour
{
    public delegate void StateChange(int state);
    public static event StateChange stateChange;

    float timer;
    bool entered = false;
    public LevelStateManager levelStateManager;
    public Camera upload;

    // Update is called once per frame
    void Update()
    {
        if(entered)
        {
            timer += Time.deltaTime;
            if(timer > 5f)
            {
                entered = false;
                upload.gameObject.SetActive(false);
                stateChange(4);
            }
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if(levelStateManager.getCurrentState() == 3)
        {
            if(col.gameObject.tag == "Player")
            {

                FindObjectOfType<AudioManager>().Stop("upload_area");
                FindObjectOfType<AudioManager>().Play("uploaded_theme");
                entered = true;
                timer = 0f;
                upload.gameObject.SetActive(true);
            }
        }
    }
}
