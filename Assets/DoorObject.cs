﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

[System.Serializable]

public class DoorObject : InteractableObject
{
    public float timeLeft = 0;
    public RaycastHit hit;
    public Transform currentDoor;
    public bool open;
    public bool isOpeningDoor;
    public Transform cam;
    public LayerMask mask;

    void Start()
    {
        uiText = GameObject.Find("InteractableText").GetComponent<Text>();
        uiImage = GameObject.Find("Crosshair").GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && timeLeft == 0.0f)
            checkDoor();
        if (isOpeningDoor)
            OpenAndCloseDoor();

    }

    public void checkDoor()
    {
        if (Physics.Raycast(cam.position, cam.forward, out hit, 5, mask))
        {
            open = false;
            if (this.transform.localRotation.eulerAngles.y > 45)
                open = true;

            isOpeningDoor = true;
            currentDoor = this.transform;
        }
    }
    public void OpenAndCloseDoor()
    {
        timeLeft += Time.deltaTime;
        if (open)
            currentDoor.localRotation = Quaternion.Slerp(currentDoor.localRotation, Quaternion.Euler(0, 0, 0), timeLeft);
        else
            currentDoor.localRotation = Quaternion.Slerp(currentDoor.localRotation, Quaternion.Euler(0, 90, 0), timeLeft);

        if (timeLeft > 1f)
        {
            timeLeft = 0;
            isOpeningDoor = false;

        }
    }
}
