﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnAI : MonoBehaviour
{

    public GameObject lecturer;

    public void OnTriggerEnter(Collider col)
    {
        lecturer.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
