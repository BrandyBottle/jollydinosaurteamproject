﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class BlueScreenTrigger : MonoBehaviour
{
    public delegate void StateChange(int state);
    public static event StateChange stateChange;

    float timer;
    bool entered = false;
    bool triggered = false;
    bool used = false;
    public LevelStateManager levelStateManager;
    public Camera upload;

    // Update is called once per frame
    void Update()
    {
        if (entered && !triggered && !used)
        {
            timer += Time.deltaTime;
            if (timer > 5f)
            {
                entered = false;
                triggered = true;
                used = true;
                upload.gameObject.SetActive(false);
            }
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (levelStateManager.getCurrentState() == 3)
        {
            if (col.gameObject.tag == "Player")
            {
                if(!used)
                {
                    entered = true;
                    timer = 0f;
                    upload.gameObject.SetActive(true);
                }
                
            }
        }
    }
}
